#include <stdio.h>

/* NOTA: modifique o código para usar as
      funções standard isalpha e toupper
   DOCUMENTAÇÃO:
      man isalpha
      man toupper
 */

int main() {
	int c;
	while ((c = getchar()) != EOF) {
		if (c >= 'a' && c <= 'z') {
			c -= 'a' - 'A';
		}
		putchar(c);
	}
	return 0;
}

