#include <stdio.h>
#include <string.h>

int readLine(char line[]) {
   unsigned len = 0;
   int c;
   while ((c = getchar()) != '\n' && c != EOF) {
      line[len++] = c;
   }
   line[len] = 0;
   return len > 0 || c != EOF;
}

#define MAX_LEN 256
#define MAX_NUM 4096

struct tnode {
   char str[MAX_LEN];
   unsigned lnode;
   unsigned rnode;
};

struct tnode lines[MAX_NUM];
unsigned cnode = 1;

void insert(unsigned cn, unsigned nn) {
   unsigned * pn;
   
   if (strcasecmp(lines[cn].str, lines[nn].str) > 0) {
      pn = &(lines[cn].lnode);
   } else {
      pn = &(lines[cn].rnode);
   }
   
   if (!*pn)
      *pn = nn;
   else
      insert(*pn, nn);
}

void show(unsigned cn) {
   if (cn) {
      show(lines[cn].lnode);
      puts(lines[cn].str);
      show(lines[cn].rnode);
   }
}

int main() {

   while (readLine(lines[cnode].str)) {
      if (cnode > 1) 
         insert(1, cnode);
      cnode += 1;
   }

   show(1);

   return 0;
}

